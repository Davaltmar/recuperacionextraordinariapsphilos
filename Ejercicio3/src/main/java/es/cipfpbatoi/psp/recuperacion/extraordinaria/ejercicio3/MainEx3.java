package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio3;

import java.util.ArrayList;

public class MainEx3 {

    public static final int TOTAL_CHARS = 25;
    
    public static void main(String[] args) {

        System.out.println("---- EJERCICIO 3 - Empezando la simulación ----");

        Estadisticas estadisticas = new Estadisticas();

        ArrayList<Thread> arrayListThreads = new ArrayList<>();

        // Con el siguiente comando, podemos crear recorrer los caracteres ascii de la a la z minúscula
        for (int j = 97; j <= 122; j++) {
                Thread characterThread = new Thread(new LetterWriter((char) j, estadisticas), String.valueOf(j));
                arrayListThreads.add(characterThread);
            }

            Thread letterKiller = new Thread(new LetterKiller(arrayListThreads, estadisticas));

            for (Thread t : arrayListThreads) {
                t.start();
            }

            letterKiller.start();

            for (Thread t : arrayListThreads) {
                try {
                    t.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        System.out.println("---- EJERCICIO 3 - La simulación ha terminado ----");

        System.out.println("\n--------------------- TOTALES -------------------\n");
//        System.out.println(estadisticas.getEstadisticas());

        System.err.println("Total masacre de letras = " + estadisticas.getTotalLetrasMuertas());
    }
}
