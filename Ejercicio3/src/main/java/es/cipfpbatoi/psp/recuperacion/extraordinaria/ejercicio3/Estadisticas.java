package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio3;

public class Estadisticas {
    private int letrasMuertas = 0;
    private int totalLetrasMuertas = 0;

//    public String getEstadisticas() {
//        StringBuilder stats = new StringBuilder();
//        stats.append("Total de vocal A = " + charA);
//        stats.append(System.lineSeparator());
//        stats.append("Total de vocal E = " + charE);
//        stats.append(System.lineSeparator());
//        stats.append("Total de vocal I = " + charI);
//        stats.append(System.lineSeparator());
//        stats.append("Total de vocal O = " + charO);
//        stats.append(System.lineSeparator());
//        stats.append("Total de vocal U = " + charU);
//        return String.valueOf(stats);
//    }

    public synchronized void addLetraMuerta(String letra) {
        letrasMuertas++;
    }

    public synchronized void addTotalLetrasMuertas() {
        totalLetrasMuertas++;
    }

    public synchronized int getTotalLetrasMuertas() {
        return totalLetrasMuertas;
    }
}
