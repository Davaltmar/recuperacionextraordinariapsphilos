package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio3;

public class LetterWriter extends Thread implements Runnable {

    Estadisticas estadisticas;
    private final char character;
    private final int numberOfIterations;
    public final int waitMsBetweenIterations;
    public static final int DEFAULT_WAIT_MS = 500;
    public static final int DEFAULT_NUMBER_OF_ITERATIONS = 10;

    public LetterWriter(char character, Estadisticas estadisticas)
    {
        this.character = character;
        this.estadisticas = estadisticas;
        this.numberOfIterations = DEFAULT_NUMBER_OF_ITERATIONS;
        this.waitMsBetweenIterations = DEFAULT_WAIT_MS;
    }

    public void printLetter() {

        for (int i = 0; i<numberOfIterations; i++){
            try {
                System.out.println(character);
                Thread.sleep(waitMsBetweenIterations);
            } catch (InterruptedException e) {
                System.err.println("Aaaah, soy la letra " + character + " y me mueroooo");
            }
        }
    }

    @Override
    public String toString() {
        return String.valueOf(getCharacter());
    }

    public char getCharacter() {
        return character;
    }

    @Override
    public void run() {
        printLetter();
    }
}
