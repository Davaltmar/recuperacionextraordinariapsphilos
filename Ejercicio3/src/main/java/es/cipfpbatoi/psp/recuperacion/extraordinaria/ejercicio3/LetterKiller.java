package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio3;

import java.util.ArrayList;
import java.util.Random;

public class LetterKiller implements Runnable {
    ArrayList<Thread> arrayListThreads;
    int timepoEntreIntentos;
    int numIntentos;
    Estadisticas estadisticas;

    public LetterKiller(ArrayList<Thread> arrayListThreads, Estadisticas estadisticas) {
        this.arrayListThreads = arrayListThreads;
        this.estadisticas = estadisticas;
        this.timepoEntreIntentos = 100;
        this.numIntentos = 30;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(timepoEntreIntentos);
            System.err.println("¡LetterKiller viene a por ti!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (numIntentos > 0) {
            for (int i = 0; i < arrayListThreads.size(); i++) {
                Random random = new Random();

                if (arrayListThreads.get(i).isAlive())
                    try {
                        int numRand = random.nextInt(100);
                        if (numRand < 75) {
                            arrayListThreads.get(i).interrupt();
                            System.err.println("Muahahaha! Me cargo la letra!");
                            numIntentos--;
                            estadisticas.addTotalLetrasMuertas();
                            Thread.sleep(timepoEntreIntentos);
                        } else {
                            numIntentos--;
                            i--;
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            }
        }
    }
}

