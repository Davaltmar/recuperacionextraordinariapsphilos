package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio1;

public class LetterWriter implements Runnable {

    private final char character;
    private final int numberOfIterations;
    public final int waitMsBetweenIterations;
    public static final int DEFAULT_WAIT_MS = 100;
    public static final int DEFAULT_NUMBER_OF_ITERATIONS = 50;

    public LetterWriter(char character){
        this(character,
                DEFAULT_NUMBER_OF_ITERATIONS,
                DEFAULT_WAIT_MS);
    }

    public LetterWriter(char character, int numberOfIterations, int waitMsBetweenIterations)
    {
        this.character = character;
        this.numberOfIterations = numberOfIterations;
        this.waitMsBetweenIterations = waitMsBetweenIterations;
    }

    public void printLetter() {
        for (int i = 0; i < numberOfIterations; i++){

            System.out.print(character);

            try {
                Thread.sleep(waitMsBetweenIterations);
//                if (i > 45) {
//                    Thread.currentThread().join();
//                }

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public String toString() {
        return String.valueOf(getCharacter());
    }

    public char getCharacter() {
        return character;
    }

    @Override
    public void run() {
        printLetter();
    }
}
