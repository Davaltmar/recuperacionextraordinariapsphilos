package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio1;

public class MainEx1 {


    public static void main(String[] args) {

        System.out.println("---- EJERCICIO 1 - Empezando la simulación ----");
        Thread[] vocalesThreads = new Thread[5];
        vocalesThreads[0] = new Thread(new LetterWriter('a'));
        vocalesThreads[1] = new Thread(new LetterWriter('e'));
        vocalesThreads[2] = new Thread(new LetterWriter('i'));
        vocalesThreads[3] = new Thread(new LetterWriter('o'));
        vocalesThreads[4] = new Thread(new LetterWriter('u'));

        for (int j = 0; j < vocalesThreads.length; j++) {
            vocalesThreads[j].start();
        }
        for (Thread t : vocalesThreads) {
            try {
                t.join();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        System.out.println("---- EJERCICIO 1 - Finalizando la simulación ----");
    }
}
