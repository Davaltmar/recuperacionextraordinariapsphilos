package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio2;

public class Estadisticas {
    private int charA = 0;
    private int charE = 0;
    private int charI = 0;
    private int charO = 0;
    private int charU = 0;
    private int totalVocales = 0;

    public synchronized void addTotalVocales() {
        totalVocales++;
    }

    public synchronized void addVocal(char character) {
        switch (character) {
            case 'a':
                charA++;
                break;

            case 'e':
                charE++;
                break;

            case 'i':
                charI++;
                break;

            case 'o':
                charO++;
                break;

            case 'u':
                charU++;
                break;
        }
    }

    public int getTotalVocales() {
        return totalVocales;
    }

    public String getEstadisticas() {
        StringBuilder stats = new StringBuilder();
        stats.append("Total de vocal A = " + charA);
        stats.append(System.lineSeparator());
        stats.append("Total de vocal E = " + charE);
        stats.append(System.lineSeparator());
        stats.append("Total de vocal I = " + charI);
        stats.append(System.lineSeparator());
        stats.append("Total de vocal O = " + charO);
        stats.append(System.lineSeparator());
        stats.append("Total de vocal U = " + charU);
        return String.valueOf(stats);
    }
}
