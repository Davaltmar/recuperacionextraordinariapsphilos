package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio2;

public class MainEx2 {

    public static void main(String[] args) {
        System.out.println("---- EJERCICIO 2 - Empezando la simulación ----");

        Estadisticas estadisticas = new Estadisticas();
        Thread[] vocalesThreads = new Thread[5];
        vocalesThreads[0] = new Thread(new LetterWriter('a', estadisticas));
        vocalesThreads[1] = new Thread(new LetterWriter('e', estadisticas));
        vocalesThreads[2] = new Thread(new LetterWriter('i', estadisticas));
        vocalesThreads[3] = new Thread(new LetterWriter('o', estadisticas));
        vocalesThreads[4] = new Thread(new LetterWriter('u', estadisticas));

        for (int j = 0; j < vocalesThreads.length; j++) {
            vocalesThreads[j].start();
        }
        for (Thread t : vocalesThreads) {
            try {
                t.join();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        System.out.println("\n---- EJERCICIO 2 - Finalizando la simulación ----\n");
        System.out.println("--------------------- TOTALES -------------------\n");
        System.out.println(estadisticas.getEstadisticas());

        System.out.println("\n" + "Total vocales = " + estadisticas.getTotalVocales());
    }
}
