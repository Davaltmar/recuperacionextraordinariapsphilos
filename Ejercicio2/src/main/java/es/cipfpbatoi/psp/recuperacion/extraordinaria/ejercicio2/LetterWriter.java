package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio2;

public class LetterWriter implements Runnable {

    private final char character;
    private final int numberOfIterations;
    public final int waitMsBetweenIterations;
    public static final int DEFAULT_WAIT_MS = 100;
    public static final int DEFAULT_NUMBER_OF_ITERATIONS = 50;
    Estadisticas estadisticas;

    public LetterWriter(char character, Estadisticas estadisticas)
    {
        this.character = character;
        this.estadisticas = estadisticas;
        this.numberOfIterations = DEFAULT_NUMBER_OF_ITERATIONS;
        this.waitMsBetweenIterations = DEFAULT_WAIT_MS;
    }

    public void printLetter() {
        for (int i = 0; i < numberOfIterations; i++){

            System.out.print(character);
            estadisticas.addTotalVocales();
            estadisticas.addVocal(character);

            try {
                Thread.sleep(waitMsBetweenIterations);
//                if (i > 45) {
//                    Thread.currentThread().join();
//                }

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public String toString() {
        return String.valueOf(getCharacter());
    }

    public char getCharacter() {
        return character;
    }

    @Override
    public void run() {
        printLetter();
    }
}
